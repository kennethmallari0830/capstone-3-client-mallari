import {useState, useEffect, useContext} from 'react'
import {Form, Button,Container} from 'react-bootstrap'
import Swal from 'sweetalert2'

//use this to redirect our user
import Router from 'next/router'

//import userContext
import UserContext from '../../userContext'

import {GoogleLogin} from 'react-google-login'


export default function Login(){

	//destructure UserContext so useContext can consume it.
	const {user, setUser} = useContext(UserContext)

	//create states for user inputs
	const [email, setEmail] = useState("");
	const [password, setPassword] = useState("");

	//create a state to conditionally rendering the button
	const [isActive, setIsActive] = useState(true)



	//useEffect that will run on initial render and will watch for the changes in our inpurt fields.
	useEffect(()=>{

		if(email !== "" && password !== ""){
			setIsActive(true)
		}else{
			setIsActive(false)
		}

	},[email, password])



	function authenticate(e){

		e.preventDefault();


		fetch('https://lit-headland-17669.herokuapp.com/api/users/login',{

			method: "POST",
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({

				email: email,
				password: password
			})
		})
		.then(res => res.json())
		.then(data =>{

			if(data.accessToken){

				//token lang ang kailangan natin
				localStorage.setItem("token",data.accessToken)

				fetch('https://lit-headland-17669.herokuapp.com/api/users/details',{

					headers:{

						Authorization: `Bearer ${data.accessToken}`
					}
				})
				.then(res=>res.json())
				.then(data =>{

					console.log(data)
					localStorage.setItem('email',data.email)
					localStorage.setItem('id', data._id)
					

					//after getting the details from the API server, we will set the global user state. Pag ka log in pa lang, iupdate na natin yung user state.
					setUser({

						email: data.email,
						id: data._id
						

					})

				})

				Swal.fire({

					icon:"success",
					title:"Successfully Logged in",
					text: "Thank you for logging in."

				})

				// Router component's push method will redirect the user to the endpoint given to the method.
				Router.push('/records')

			}else{

				Swal.fire({

					icon: "error",
					title: "Unsuccessful Login",
					text: "User authenticaltion has Failed."
				})
			}
			
		})

		//set the  input states into their initial value
		setEmail("");
		setPassword("");

	}

	function authenticateGoogleToken(response){
		console.log(response)
		fetch('https://lit-headland-17669.herokuapp.com/api/users/verify-google-id-token', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json' 
			}, 
				body: JSON.stringify({
					tokenId: response.tokenId,
					accessToken: response.accessToken
				})
		}) 
		.then(res => res.json())
		.then(data => {

			console.log(data)
			if(typeof data.accessToken !== 'undefined'){
				localStorage.setItem('token', data.accessToken)
				fetch('https://lit-headland-17669.herokuapp.com/api/users/details', {
					headers: {
						'Authorization': `Bearer ${data.accessToken}`
					}
				})
				.then(res => res.json())
				.then(data => {
					localStorage.setItem('email', data.email)
					localStorage.setItem('id', data._id)

					setUser({
						email: data.email,
						id: data._id
					})
					Swal.fire({
						icon: 'success',
						title: 'Successful Login'
					})
					Router.push('/records')
				})
			} else {
				if(data.error === "google-auth-error"){
					Swal.fire({
						icon: 'error',
						title: 'Google Authentication Failed'
					}) 
				} else if(data.error === "login-type-error") {
					Swal.fire({
						icon: 'error',
						title: 'Login failed',
						text: 'You may have registered through a different procedure'
					})
				}
			}
		})
	}




	return(

			<Container>
			  <Form onSubmit={e => authenticate(e)} className="my-3">
					<Form.Group controlId="userEmail">
						<Form.Label>Email:</Form.Label>
						<Form.Control type="email" placeholder="Enter Email" value={email} onChange={e=> setEmail(e.target.value)} required />
					</Form.Group>
					<Form.Group controlId="userPassword">
						<Form.Label>Password:</Form.Label>
						<Form.Control type="password" placeholder="Enter Email" value={password} onChange={e=> setPassword(e.target.value)} required/>
					</Form.Group>

				   {
						isActive

						?
						<Button variant="primary" type="submit" className="btn-block">Submit</Button>
						:
						<Button variant="primary" type="submit" disabled className="btn-block">Submit</Button>

				    }

				    <GoogleLogin 
				    	clientId="168595206810-02estdan10lnj23o9esplutbvlvtg1t9.apps.googleusercontent.com" 
				    	buttonText="Google Login"
				    	onSuccess={authenticateGoogleToken}
				    	onFailure={authenticateGoogleToken}
				    	cookiePolicy={'single_host_origin'}
				    	className="w-100 text-center d-flex justify-content-center my-2 rounded-lg"	/>
		     </Form>
		 </Container>

	    )

}