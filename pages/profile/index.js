import {useState, useEffect, useContext} from 'react'
import {Form, Button, Container, Row, Col, Card, Jumbotron} from 'react-bootstrap'
import Swal from 'sweetalert2'

import Router from 'next/router'
//import user context
import UserContext from '../../userContext'



export default function Profile(){

	const {user} = useContext(UserContext)
	console.log(user)

//State for token

const [userToken, setUserToken] = useState({

	setUserToken:null
})

//useEffect for the Token

      useEffect(()=>{

      setUserToken(localStorage.getItem('token'))


    },[])

      console.log(userToken)


//State for the User details
const [userRecord, setUserRecord] = useState([])

//useEffect for User Details
      useEffect(()=>{

      	fetch('https://lit-headland-17669.herokuapp.com/api/users/details',{

					headers:{

						Authorization: `Bearer ${localStorage.getItem('token')}`
					}
				})
      			.then(res=>res.json())
				.then(data =>{

					console.log(data)
					setUserRecord(data)
					

				})
      	

    },[])

      console.log(userRecord)



	return(

			<>

				
				<Row>
					<Col md={12}>
						<Card className="cardHighlight my-5">
							<Card.Body className="text-center">
								<Card.Header className="profile-header"><h4>User Profile</h4></Card.Header>
								<Card.Title>
									<h5>First Name: {userRecord.firstName}</h5>
								</Card.Title>
								<Card.Text>
								 	<h5>Last Name: {userRecord.lastName}</h5>
								</Card.Text>
								<Card.Text>
								 	<h5>Email: {userRecord.email}</h5>
								</Card.Text>
								<Card.Text>
								 	<h5>Current Balance: PHP {userRecord.balance}</h5>
								</Card.Text>
								
							</Card.Body>
						</Card>
					</Col>
				</Row>
		  </>
			
		)

}



/*

	<Jumbotron className="text-center">
				<h2 className="my-3">User Profile</h2>
				<h5>First Name:</h5>{userRecord.firstName}
				<h5>Last Name:</h5>{userRecord.lastName}
				<h5>Email:</h5>{userRecord.email}
				<h5>Current Balance:</h5>PHP: {userRecord.balance}
			</Jumbotron>


*/


			
