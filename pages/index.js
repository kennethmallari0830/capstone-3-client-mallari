import {Fragment} from 'react'

//components
import Banner from '../components/Banner'
import Highlights from '../components/Highlights'

export default function Home() {

  const data={

    title: "Sage.ly",
    content: "Budget Tracker for everyone",
    destination:"/register",
    label: "Sign Up"
  }

  return (
    <Fragment>
      <Banner dataProp={data}/>
      <Highlights />
    </Fragment>
    
  )
}
