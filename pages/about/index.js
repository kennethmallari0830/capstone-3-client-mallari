import {Jumbotron, Button, Container, Row, Column} from 'react-bootstrap'
import Image from 'next/image'

export default function About(){
	return(

		<>
			<Jumbotron className="about-jumbo">
				<h1 className="text-center">The Sage.ly Story</h1>
				<h5 className="text-center">Budget Tracker for everyone.</h5>
			</Jumbotron>
			
			<Container className="my-4 text-center">
			    <>
						<Image className="pictureKo"
							src="/trypic.jpg"
							alt="Picture of the developer"
							 width={250}
        					height={250}
						/>
						<p className="text-justify my-4">
							Hi, i'm Kenneth Christian. I am the developer of Sage.ly. I love watching travel vlogs as I dream to travel the world in the near future. I believe in the importance of budgetting. As they say, "budgeting allows us to create a spending plan for our money, it ensures that we will always have enough money for the things we need and the things that are important to us". With effective budget tracking, you know where your money goes and you can ensure that your money is used wisely.  Yes, there are a lot of ways to track our spending like pen and paper, and spreadsheets. But using these methods usually includes tedious amount of work. There are plethora of online apps out there but most often than not,  you need to pay so you can use them. 
						</p>
						<p className="text-justify">
							Tracking one's budget should not be this hard! That inspired me to create this web app. My aim is to develop a free program so people could track their spending in a much easier way. They can say goodbye to pen and paper, spreadsheet and somehow eliminate the need to give their hard earned money for paid apps just to track their finances. The app includes some of the most important things in budget tracking like the ability to record income or expense transaction, keep track of their current balance, search their transactions, etc. It also includes data visualization like graphs so users an have a better overview of their spending trends. I hope this app could help thousands of individuals in streamlining their finances tracking so we could be a better stewards of our money.
						</p>
			   </>
	   		</Container>
	   </>
	   

		)
}