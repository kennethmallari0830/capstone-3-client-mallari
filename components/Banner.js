import {Jumbotron, Row, Col} from 'react-bootstrap'
import {useContext} from 'react'

import Link from 'next/link'
import UserContext from '../userContext'

export default function Banner({dataProp}){
	//destructure
	const {title,content, destination,label} = dataProp

	const {user} = useContext(UserContext)
	console.log(user)


	return(

			<Row>
				<Col>
					<Jumbotron className="jumbo">
					<h1>{title}</h1>
					<h3>{content}</h3>
					<Link  href={destination}><a className="btn btn-info">{label}</a></Link>
				</Jumbotron>			
				</Col>
			</Row>



		)
}
















// import {Jumbotron, Row, Col} from 'react-bootstrap'



// import Link from 'next/link'
// export default function Banner({dataProp}){

	

// 	//destructure
// 	const {title,content, destination,label} = dataProp


// 	return(

// 			<Row>
// 				<Col>
// 					<Jumbotron>
// 					<h1>{title}</h1>
// 					<h3>{content}</h3>
// 					<Link  href={destination}><a className="btn btn-primary">{label}</a></Link>
// 				</Jumbotron>			
// 				</Col>
// 			</Row>



// 		)
// }