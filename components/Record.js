import{useState, useEffect, useContext} from 'react'
import {Card} from 'react-bootstrap'
// used to manipulate JS time format
import moment from 'moment' 


// To get the prop, destructure it
export default function Category({recordProp}){

	//  after you get the prop, destructure it para makuha mo yng laman na need mo:
	let {_id, name, description, type,category, balance, amount, recordedOn} = recordProp
	
	let bago = (moment(recordedOn).format('LL'))

	return(

			<Card className="my-3">
				<Card.Body>
					<Card.Title>
					Type: {type}
					</Card.Title>
					<Card.Title>
					Category: {category}
					</Card.Title>
					<Card.Text>
					Description: {description}
					</Card.Text>
					<Card.Text>
					Transaction Amount: {amount}
					</Card.Text>
					<Card.Text>
					Current Balance: {balance}
					</Card.Text>
					<Card.Text>
					Date: {bago}
					</Card.Text>
				</Card.Body>
			</Card>
		)
}