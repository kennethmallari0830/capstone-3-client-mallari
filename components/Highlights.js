import {Row,Col,Card, Container} from 'react-bootstrap'

export default function Highlights(){

	return(

		   
			<Row className="cardHighlights">
				<Col xs={12} md={4}>
					<Card className="cardHighlight">
						<Card.Body>
							<Card.Title>
							<h2>Be on top of your budget.</h2>
							<Card.Text>
							Say goodbye to spreadsheets and paper for tracking your personal finances. 
							</Card.Text>
							</Card.Title>
						</Card.Body>
					</Card>
				</Col>
				<Col xs={12} md={4}>
					<Card className="cardHighlight">
						<Card.Body>
							<Card.Title>
							<h2>Free to Use.</h2>
							<Card.Text>
							This web app is for free. Just register and you are ready to go! 
							</Card.Text>
							</Card.Title>
						</Card.Body>
					</Card>
				</Col>
				<Col xs={12} md={4}>
					<Card className="cardHighlight">
						<Card.Body>
							<Card.Title>
							<h2>Includes Data Visualization</h2>
							<Card.Text>
							Stay on top of your spending, and budget as this app includes graphs for easier data visualization.
							</Card.Text>
							</Card.Title>
						</Card.Body>
					</Card>
				</Col>
			</Row>
		



		)
}