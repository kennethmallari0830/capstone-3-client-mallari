import {Fragment, useContext} from 'react'
import {Navbar,Nav} from 'react-bootstrap'
import Link from 'next/link'


import UserContext from '../userContext'

export default function NavBar(){

	//destructure UserContext
	const {user} = useContext(UserContext)
	console.log(user)

	return(

			<Navbar bg="primary" expand="lg" variant="dark">
					<Link href='/'>
						<a className="navbar-brand">Sage.ly</a>
					</Link>
					<Navbar.Toggle aria-controls="basic-navbar-nav" />
					<Navbar.Collapse>
						<Nav className="ml-auto">
							

							{

								user.email
								?
								 <Fragment>
									<Link href='/categories'>
										<a className="nav-link" role="button">Category</a>
									</Link>
									<Link href='/records'>
										<a className="nav-link" role="button">Record</a>
									</Link>
									<Link href='/search'>
										<a className="nav-link" role="button">Search</a>
									</Link>
									<Link href='/charts'>
										<a className="nav-link" role="button">Charts</a>
									</Link>
									<Link href='/profile'>
										<a className="nav-link" role="button">Profile</a>
									</Link>
									<Link href="/logout">
										<a className="nav-link" role="button">Log out</a>
						    		</Link>
						    	</Fragment>
							    :
							     <Fragment>

							     	<Link href='/'>
										<a className="nav-link" role="button">Home</a>
									</Link>

							    	<Link href='/login'>
										<a className="nav-link" role="button">Log in</a>
									</Link>
							
									<Link href='/register'>
										<a className="nav-link" role="button">Register</a>
									</Link>
									<Link href='/about'>
										<a className="nav-link" role="button">About</a>
									</Link>
								</Fragment>

							}

							
							
							
							
							
							
							
						</Nav>
					</Navbar.Collapse>
				</Navbar>


		)
}